function renderChart (data, labels) {
  var ctx = document.getElementById('myChart').getContext('2d')
  var myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: labels,
      datasets: [{
        label: 'Talajnedvesség',
        data: data,
      }]
    },
  })
}

