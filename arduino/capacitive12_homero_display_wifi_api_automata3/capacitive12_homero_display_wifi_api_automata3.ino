#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>     // https://github.com/tzapu/WiFiManager
#include <Keypad.h>
#include <ArduinoJson.h>

#include "DHT.h"
#include "TM1651.h"
#include "ConfigTool.h"

struct Config {
  int watering = 570;
  int watering_pump1_time = 20000;
  int watering_pump2_time = 20000;
};

// NET
const char* ssid = "";
const char* password = "";
//#define WLAN_SSID   "INFOLL"
//#define WLAN_PASS   "1NF0llKFT"
#define WLAN_SSID   "ALHN-5A6F"
#define WLAN_PASS   "7166181084"

//const char* host = "192.168.1.67";
const char* host = "donquijote.hu";


// Battery display
#define CLK 14 //D5 4->D2 pins definitions for TM1651 and can be changed to other ports       
#define DIO 12 //D6 0->D3 

#define DHTPIN 13     //D7 2->D4 Digital pin connected to the DHT sensor
#define DHTTYPE DHT11   // DHT 11

// motor 1
#define DIRA 0
#define PWMA 5
#define DIRB 2
#define PWMB 4


DHT dht(DHTPIN, DHTTYPE);
TM1651 batteryDisplay(CLK, DIO);

int timeapi;

Config config;
ConfigTool configTool;

void setup() {
  Serial.begin(9600); // open serial port, set the baud rate as 9600 bps

  configTool.addVariable("watering", &config.watering);
  configTool.addVariable("watering_pump1_time", &config.watering_pump1_time);
  configTool.addVariable("watering_pump2_time", &config.watering_pump2_time);

  dht.begin();


  batteryDisplay.init();
  batteryDisplay.set(BRIGHTEST);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  batteryDisplay.frame(FRAME_ON);


  // WIFI

  WiFiManager wifiManager;
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  int value;
  value = 1;
  while (WiFi.status() != WL_CONNECTED && value < 20 ) {
    delay(500);
    Serial.print(".");
    batteryDisplay.displayLevel(value % 7);
    value = value + 1;
  }
  Serial.println();

  Serial.println("WiFi sikeresen csatlakoztatva");
  Serial.println("IP cím: "); Serial.println(WiFi.localIP());

  // preparing motor
  Serial.println("Preparing motor...");
  pinMode(DIRA, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(DIRB, OUTPUT);
  pinMode(PWMB, OUTPUT);

  analogWrite(PWMA, 0);
  digitalWrite(DIRA, 1);
  analogWrite(PWMB, 0);
  digitalWrite(DIRB, 1);
  timeapi =  0;
  delay(5000);
}
void loop() {
  int val;
  int value;
  int w = 0;
  val = analogRead(0); //connect sensor to Analog 0
  Serial.println(val); //print the value to serial port

  // HOMERO
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    //return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(F("Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));

  //delay(5000);
  //value = 8 - round(val - 420) / 20;
  value = 8 - round(val - 367) / 52;
  if (value < 0) {
    value = 0;
  }
  Serial.println(value);
  batteryDisplay.displayLevel(value);

  if (timeapi <=  0) {
    // REST API START
    if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

      StaticJsonBuffer<300> JSONbuffer;   //Declaring static JSON buffer
      JsonObject& JSONencoder = JSONbuffer.createObject();

      JSONencoder["value"] = val;
      JSONencoder["humidity"] = h;
      JSONencoder["temperature"] = t;
      JSONencoder["heatindex"] = hic;
      char JSONmessageBuffer[300];
      JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
      Serial.println(JSONmessageBuffer);

      HTTPClient http;    //Declare object of class HTTPClient
      // DONQUIJOTE
      http.begin("http://donquijote.hu/hibi/public/api/measures/add");      //Specify request destination
      //http.begin("http://192.168.1.67/hibi/public/api/measures/add");      //Specify request destination
      http.addHeader("Content-Type", "application/json");  //Specify content-type header
      //String PostData = 'value="ss"';
      //http.addHeader("Content-Type", "text/plain");  //Specify content-type header


      // Serial.println(JSONmessageBuffer);

      int httpCode2 = http.POST(JSONmessageBuffer);   //Send the request
      String payload2 = http.getString();                  //Get the response payload
      JsonObject& root = JSONbuffer.parseObject(payload2);
      const char* code = root["notice"];
      const char* watering = root["watering"].as<char*>();
      Serial.print("Code return element = ");
      Serial.println(code);
      Serial.println(httpCode2);   //Print HTTP return code
      Serial.println(payload2);    //Print request response payload
      Serial.println(watering);
      http.end();  //Close connection
      w = atoi((char *)watering);
      Serial.println(w);
      const char* watering1 = root["watering1"].as<char*>();
      Serial.println(watering1);
      const char* watering2 = root["watering2"].as<char*>();
      Serial.println(watering2);
      String payload3 = "{watering:1}";
    } else {

      Serial.println("Error in WiFi connection");

    }
    // REST API END

    // locsolás
    timeapi = 60;

    if (w == 1 || w == 2 || w == 3 || val > 570 ) {
      motor_control(w, val, config.watering, config.watering_pump1_time, config.watering_pump2_time);
      //  configTool.save();
      w = 0;
      timeapi = 600;
    };

  } else {
    timeapi = timeapi - 1;
  }

  Serial.println("Ciklus:");
  Serial.println(timeapi);
  delay(1000);

}

void motor_control(int id, int val, int watering, int watering_pump1_time, int watering_pump2_time)
{
  batteryDisplay.displayLevel(8);
  delay(500);
  batteryDisplay.displayLevel(7);
  delay(500);
  batteryDisplay.displayLevel(6);
  delay(500);
  batteryDisplay.displayLevel(5);
  delay(500);
  batteryDisplay.displayLevel(4);
  delay(500);
  batteryDisplay.displayLevel(3);
  delay(500);  
  batteryDisplay.displayLevel(2);
  delay(500);
  batteryDisplay.displayLevel(1);
  delay(500);  
  batteryDisplay.displayLevel(0);
  delay(500);  
  
  if (id == 1 || id == 3 || val > watering) {
    Serial.println(watering);
    Serial.println("motor1 be");
    Serial.println(watering_pump1_time);
    analogWrite(PWMA, 1023);
    delay(watering_pump1_time);
    analogWrite(PWMA, 0);
    Serial.println("motor1 ki");
  }
  if (id == 2 || id == 3 || val > watering) {
    Serial.println("motor2 be");
    Serial.println(watering_pump2_time);
    analogWrite(PWMB, 1023);
    delay(watering_pump2_time);
    analogWrite(PWMB, 0);
    Serial.println("motor2 ki");
  }
  delay(1000);
}
