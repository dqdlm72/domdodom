<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// Get All Customers
$app->get('/api/customers', function(Request $request, Response $response){
    $sql = "SELECT * FROM customers";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customers);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Get All Customers
$app->get('/api/measures', function(Request $request, Response $response){
    $sql = "SELECT * FROM measures";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customers);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Get Single Customer
$app->get('/api/customer/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');

    $sql = "SELECT * FROM customers WHERE id = $id";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customer);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Add Customer
$app->post('/api/customer/add', function(Request $request, Response $response){
    $first_name = $request->getParam('first_name');
    $last_name = $request->getParam('last_name');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $state = $request->getParam('state');

    $sql = "INSERT INTO customers (first_name,last_name,phone,email,address,city,state) VALUES
    (:first_name,:last_name,:phone,:email,:address,:city,:state)";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name',  $last_name);
        $stmt->bindParam(':phone',      $phone);
        $stmt->bindParam(':email',      $email);
        $stmt->bindParam(':address',    $address);
        $stmt->bindParam(':city',       $city);
        $stmt->bindParam(':state',      $state);

        $stmt->execute();

        echo '{"notice": {"text": "Customer Added"}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Get lastMeasure
$app->get('/api/measures/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    if ($id === null) {
        $id=1;
    }

    $sql = "SELECT * FROM measures WHERE  device_id = $id ORDER BY id DESC";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customer);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Add Measures
$app->post('/api/measures/add', function(Request $request, Response $response){
    $value = $request->getParam('value');
    $device =1;
    $measure_type_id=1;
    //$value = $request->getParsedBody();

    $sql0 = 'SELECT * FROM watering WHERE device_id='.$device. ' ORDER BY id DESC LIMIT 1 ';
    $watering= 0;
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql0);
        $m = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        $watering= $m->watering;
    } catch(PDOException $e){
        $watering= 0;
    }
    $sql0 = 'UPDATE watering SET watering =0 ORDER BY id DESC LIMIT 1';
    try{
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql0);
    } catch(PDOException $e){
     
    }    
    
    $d = date('Y-m-d H',time()).':00:00';
    //$d = date('Y-m-d h:i',time()).':00';
    $sql1 = 'SELECT * FROM measures WHERE device_id='.$device. ' and measure_type_id='.$measure_type_id. ' and date = "'.$d.'"';
    
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql1);
        $m = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //echo json_encode($m->id);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }

if ($m->id === null) {
    $sql = "INSERT INTO measures (max_value,value,date) VALUES
    (:max_value,:value,:date)";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':value', $value);
        $stmt->bindParam(':max_value', $value);
        $stmt->bindParam(':date',  $d);
        $stmt->execute();

        echo '{watering:'. $watering.'}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage(). $value.'}';
    }
} else {
    $minvalue = $value < $m->value ? $value : $m->value;
    $maxvalue = $value > $m->max_value ? $value : $m->max_value;
    
    $sql = "UPDATE measures SET 
    value = :value, max_value = :max_value
    WHERE id = $m->id";    
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':value', $minvalue);
        $stmt->bindParam(':max_value', $maxvalue);
        $stmt->execute();

        echo '{watering:'. $watering.'}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage(). $value.'}';
    }

}

});



// Update Customer
$app->put('/api/customer/update/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
    $first_name = $request->getParam('first_name');
    $last_name = $request->getParam('last_name');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $city = $request->getParam('city');
    $state = $request->getParam('state');

    $sql = "UPDATE customers SET
				first_name 	= :first_name,
				last_name 	= :last_name,
                phone		= :phone,
                email		= :email,
                address 	= :address,
                city 		= :city,
                state		= :state
			WHERE id = $id";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':first_name', $first_name);
        $stmt->bindParam(':last_name',  $last_name);
        $stmt->bindParam(':phone',      $phone);
        $stmt->bindParam(':email',      $email);
        $stmt->bindParam(':address',    $address);
        $stmt->bindParam(':city',       $city);
        $stmt->bindParam(':state',      $state);

        $stmt->execute();

        echo '{"notice": {"text": "Customer Updated"}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Delete Customer
$app->delete('/api/customer/delete/{id}', function(Request $request, Response $response){
    $id = $request->getAttribute('id');

    $sql = "DELETE FROM customers WHERE id = $id";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;
        echo '{"notice": {"text": "Customer Deleted"}';
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// watering on
$app->post('/api/watering/on', function(Request $request, Response $response){
 
    $sql = "INSERT INTO watering (device_id,watering) VALUES (1,1)";
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':value', $minvalue);
        $stmt->bindParam(':max_value', $maxvalue);
        $stmt->execute();

        echo '{watering:1}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage(). $value.'}';
    }
});
