<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// Get all measures
$app->get('/api/measures', function (Request $request, Response $response) {
    $sql = "SELECT * FROM measures";
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customers);
    } catch (PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


// Get measure
$app->get('/api/measures/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    if ($id === null) {
        $id=1;
    }
    $sql = "SELECT * FROM measures WHERE  device_id = $id ORDER BY id DESC";
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customer);
    } catch (PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Add Measures
$app->post('/api/measures/add', function (Request $request, Response $response) {
    $value = $request->getParam('value');
    $humidity=intval($request->getParam('humidity'));
    $temperature=intval($request->getParam('temperature'));
    $heatindex=intval($request->getParam('heatindex'));
    $device =1;
    $measure_type_id=1;
    $sql0 = 'SELECT * FROM devices WHERE id='.$device. ' ORDER BY id DESC LIMIT 1 ';
    $watering1= 0;
    $watering2= 0;
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql0);
        $m = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        $watering1= $m->watering1;
        $watering2= $m->watering2;
    } catch (PDOException $e) {
        $watering1= 0;
        $watering2= 0;
    }

    $sql0 = 'SELECT * FROM watering WHERE device_id='.$device. ' ORDER BY id DESC LIMIT 1 ';
    $watering= 0;
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql0);
        $m = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        $watering= $m->watering;
    } catch (PDOException $e) {
        $watering= 0;
    }

    $sql0 = 'UPDATE watering SET watering =0 ORDER BY id DESC LIMIT 1';
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql0);
    } catch (PDOException $e) {
    }
    $d = date('Y-m-d H', time()).':00:00';
    $sql1 = 'SELECT * FROM measures WHERE device_id='.$device. ' and measure_type_id='.$measure_type_id. ' and date = "'.$d.'"';
    
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql1);
        $m = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
    } catch (PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
    if ($m->id === null) {
        $sql = "INSERT INTO measures (max_value,value,date, humidity, temperature, heatindex) VALUES
    (:max_value,:value,:date, :humidity, :temperature, :heatindex)";

        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':value', $value);
            $stmt->bindParam(':max_value', $value);
            $stmt->bindParam(':date', $d);
            $stmt->bindParam(':humidity', $humidity);
            $stmt->bindParam(':temperature', $temperature);
            $stmt->bindParam(':heatindex', $heatindex);
            $stmt->execute();

            echo '{"watering":'. $watering.',"watering1":'. $watering1.',"watering2":'. $watering2.'}';
        } catch (PDOException $e) {
            echo '{"error": {"text": '.$e->getMessage(). $value.'}';
        }
    } else {
        $minvalue = $value < $m->value ? $value : $m->value;
        $maxvalue = $value > $m->max_value ? $value : $m->max_value;
        $sql = "UPDATE measures SET 
    value = :value, 
    max_value = :max_value,
    humidity = :humidity, 
    temperature= :temperature, 
    heatindex = :heatindex 
    WHERE id = $m->id";
        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':value', $minvalue);
            $stmt->bindParam(':max_value', $maxvalue);
            $stmt->bindParam(':humidity', $humidity);
            $stmt->bindParam(':temperature', $temperature);
            $stmt->bindParam(':heatindex', $heatindex);
            $stmt->execute();

            echo '{watering:'. $watering.',watering1:'. $watering1.',watering2:'. $watering2.'}';
        } catch (PDOException $e) {
            echo '{"error": {"text": '.$e->getMessage(). $value.'}';
        }
    }
});

// watering on
$app->post('/api/watering/on', function (Request $request, Response $response) {
    $id = $request->getParam('id');
    $sql = "INSERT INTO watering (device_id,watering) VALUES (1,$id)";
    try {
        $db = new db();
        $db = $db->connect();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':value', $minvalue);
        $stmt->bindParam(':max_value', $maxvalue);
        $stmt->execute();
        echo '{watering:...'.$id.'}';
    } catch (PDOException $e) {
        echo '{"error": {"text": '.$e->getMessage(). $value.'}';
    }
});
